global _start

section .text

__start:
_start:
  mov rax, 1        
  mov rdi, 1        
  mov rsi, msg      
  mov rdx, 9  
  syscall

  mov rax, 0
  mov rsi, rsp
  mov rdi, 0
  mov rdx, 400
  syscall
  ret

  shl rax, 1
  ret

  mov rcx, 1
  xor rax, rcx
  ret

  xor rax, rax
  ret

section .data
  msg: db "Please Sav", 0
  sh: db "/bin/sh", 0
