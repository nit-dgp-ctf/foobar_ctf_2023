#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>

char a[] = "/bin/sh";
void armory()
{
    __asm__("add x0, x0, #0x1;");
    __asm__("ldp x1, lr, [sp, #-16];");
    __asm__("ret;");

    __asm__("eor x0, x0, x0;");
    __asm__("ldr lr, [sp];");
    __asm__("ret;");

    __asm__("ldp x8, x9, [sp, #16];");
    __asm__("cmp x0, x8;");
    __asm__("ldr x8, [sp, #32]");
    __asm__("ldr lr, [sp, #48];");
    __asm__("b.eq repeat;");
    __asm__("ret;");
    __asm__("repeat:");
    __asm__("ret x8;");
}

int dealer()
{
    char buffer[128];
    printf("Hi I am, ");
    system("whoami");
    printf("Identify Yourself (alias): ");
    gets(buffer);
    fflush(stdout);
}

int main(int argc, char **argv)
{
    dealer();
    return EXIT_SUCCESS;
}