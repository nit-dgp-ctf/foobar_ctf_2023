#include <stdio.h>
#include <stdlib.h>
#include <errno.h>
#include <string.h>
#include <sys/mman.h>
#include <unistd.h>
#include "helper.h"

typedef unsigned int DWORD;

int check(char *password, int len)
{
    int dispatcher_val = 0;
    int idx = 0;
    int status[] = {0, 1};

    int result[25];
    char a[] = {77, 88, 75, 111, 73, 122, 10, 17, 29, 59, 39, 43, 39, 47, 59, 28, 17, 13, 121, 109, 106, 76, 91, 119};
    // 77, 88, 75, 111, 73, 94, 3, 15, 13, 36, 26, 61, 42, 49, 9, 35, 15, 0, 78, 91, 109, 112, 80, 119
    while (1)
    {
        switch (dispatcher_val)
        {
        case 0x0:
            if (len < 20)
                return 1;
            dispatcher_val = 0x1;
            continue;
        case 0x1:
            result[0] = password[0] ^ 10;
            result[1] = password[1] ^ 20;
            result[2] = password[2] ^ 30;
            result[3] = password[3] ^ 40;
            result[4] = password[4] ^ 50;
            result[5] = password[5] ^ 60;
            result[6] = password[6] ^ 70;
            result[7] = password[7] ^ 80;
            result[8] = password[8] ^ 90;
            result[9] = password[9] ^ 100;
            result[10] = password[10] ^ 110;
            result[11] = password[11] ^ 120;
            result[12] = password[12] ^ 120;
            result[13] = password[13] ^ 110;
            result[14] = password[14] ^ 100;
            result[15] = password[15] ^ 90;
            result[16] = password[16] ^ 80;
            result[17] = password[17] ^ 70;
            result[18] = password[18] ^ 60;
            result[19] = password[19] ^ 50;
            result[20] = password[20] ^ 40;
            result[21] = password[21] ^ 30;
            result[22] = password[22] ^ 20;
            result[23] = password[23] ^ 10;
            dispatcher_val = 0x2;
            continue;
        case 0x2:
            for (int i = 0; i < 24; i++)
                result[i] ^= a[i];

            if (result[0] |
                result[1] |
                result[2] |
                result[3] |
                result[4] |
                result[5] |
                result[6] |
                result[7] |
                result[8] |
                result[9] |
                result[10] |
                result[11] |
                result[12] |
                result[13] |
                result[14] |
                result[15] |
                result[16] |
                result[17] |
                result[18] |
                result[19] |
                result[20] |
                result[21] |
                result[22] |
                result[23])
                idx = 1;

            return status[idx];
        }
    }
}

int change_page_permissions_of_address(void *addr)
{
    // Move the pointer to the page boundary
    int page_size = getpagesize();
    addr -= (unsigned long)addr % page_size;
    return mprotect(addr, page_size * 2, PROT_READ | PROT_WRITE | PROT_EXEC);
}

DWORD ex(void *function)
{
    DWORD *fnA;
    fnA = (DWORD *)function;
    if (change_page_permissions_of_address(fnA) == -1)
    {
        printf("Error");
        exit(1);
    };
    return 0;
}

void __wm__(DWORD StartAddr, DWORD offset, char num)
{
    __asm__(
        "push eax;"
        "push ecx;"
        "mov ecx, [%[StartAddr]];"
        "add ecx, [%[offset]];"
        "mov al, [%[num]];"
        "mov byte ptr [ecx], al;"
        "pop ecx;"
        "pop eax;" ::[StartAddr] "g"(StartAddr),
        [offset] "g"(offset), [num] "g"(num));
}

void __gmon_start__()
{
    ex((DWORD *)&check);
    __wm__((DWORD *)&check, 0x78, 0x77);
    __wm__((DWORD *)&check, 0x5b, 0x3);
    __wm__((DWORD *)&check, 0x62, 0x1a);
    __wm__((DWORD *)&check, 0x5c, 0xf);
    __wm__((DWORD *)&check, 0x60, 0xd);
    __wm__((DWORD *)&check, 0x69, 0x9);
    __wm__((DWORD *)&check, 0x5a, 0x5e);
    __wm__((DWORD *)&check, 0x77, 0x50);
    __wm__((DWORD *)&check, 0x63, 0x3d);
    __wm__((DWORD *)&check, 0x68, 0x31);
    __wm__((DWORD *)&check, 0x6e, 0xf);
    __wm__((DWORD *)&check, 0x61, 0x24);
    __wm__((DWORD *)&check, 0x6f, 0x0);
    __wm__((DWORD *)&check, 0x67, 0x2a);
    __wm__((DWORD *)&check, 0x75, 0x6d);
    __wm__((DWORD *)&check, 0x70, 0x4e);
    __wm__((DWORD *)&check, 0x71, 0x5b);
    __wm__((DWORD *)&check, 0x76, 0x70);
    __wm__((DWORD *)&check, 0x6a, 0x23);
}

void init()
{
    __gmon_start__();
}

int main(int argc, char **argv)
{
    // static flag: GLUG{FLAG_IS_A_FAKE_BRO}
    char buffer[50];
    int page_size = getpagesize();
    init();
    scanf("%50s", buffer);
    // hexDump("\nUnmodified", &check, page_size);
    // __wm__((DWORD *)&check, x, 20);
    if (check(buffer, strlen(buffer)) != 0)
        printf("Empty your mind, be more formless");
    else
        printf("Conquer the path ahead of you");
    // hexDump("Modified", &check, page_size);
    fflush(stdout);

    return 0;
}
