OFFSET = 8
NODES = 1000

import base64

call_graph = {}
dest = ""
src = "f1"

with open("./mappings.go") as f:
    for i in range(OFFSET):
        f.readline()
    for i in range(NODES):
        z = f.readline().strip()
        left, right = z.split("=")
        left = left.replace("var","").strip()
        right = right.replace("findMeFirst(","")
        right = right.replace(")","")
        right = right.split(", ")
        for i in range(len(right)):
            right[i] = right[i].strip()

        # print(left, right)
        if "f0" in right:
            call_graph[left] = []
        else:
            if "" in right:
                dest = left
            call_graph[left] = right

# for i in call_graph.keys():
#     print(f'{i}: {call_graph[i]}')

def findPath():
    queue = []                                          
    visited = set()                                    
    queue.append((src, ''))                             
    x= 0
    payload = b''
    while len(queue) > 0:                               
        x+=1
        first, path = queue[0]
        if first == dest:                               
            # print(path)
            for i in path:
                num = int(i)
                encode = b'Haha_you_came'
                for j in range(num):
                    encode = base64.b64encode(encode)
                payload += encode + b'|'
            break
        visited.add(first)                              
        for index in range(len(call_graph[first])):     
            reachable = call_graph[first][index]
            if reachable not in visited:
                char = f'{index}' 
                queue.append((reachable, path + char))  

        del queue[0]

    return payload

print(findPath()[:-1].decode())
