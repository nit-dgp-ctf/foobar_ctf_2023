#include <jni.h>
#include <cstdlib>
#include <ctime>
#include <string>
#include <vector>

class VM
{
public:
    uint32_t reg[32]{}; // 32 registers will be present for our system
    uint32_t pc{}; // Instruction pointer or program counter
    std::vector<uint8_t> memory; // corresponds to memory

    void execMicroMine(); // responsible for executing individual mine instructions
    int mine(); // Mines on device given the mineHash

};

int VM::mine() {
    // using register 9 for end
    // using register 10 for errors
    reg[9] = 1; // start
    reg[10] = 0; // no errors
    while(reg[9] && !reg[10])
    {
        try {
            execMicroMine();
        } catch (const std::exception& ex) {
            reg[10] = 1;
        }
    }

    return ((reg[10]<<1)|reg[0]);
}

void VM::execMicroMine() {

    switch(memory[pc]) {
        case 0x00: {
            // XOR r1, r2
            reg[memory[pc+1]] ^= reg[memory[pc+2]];
            pc+=3;
            return;
        }


        case 0x01: {
            // AND r1, r2
            reg[memory[pc+1]] &= reg[memory[pc+2]];
            pc+=3;
            return;
        }


        case 0x02: {
            // OR r1, r2
            reg[memory[pc+1]] |= reg[memory[pc+2]];
            pc+=3;
            return;
        }


        case 0x03: {
            // MOV r1, r2
            reg[memory[pc+1]] = reg[memory[pc+2]];
            pc+=3;
            return;
        }


        case 0x04: {
            // MOVI r1, val
            memcpy(&reg[memory[pc+1]],&memory[pc+2],4);

            pc+=6;
            return;
        }

        case 0xff: { // END
            reg[9] = 0;
            return;
        }
    }

    reg[10] = 1;
}

extern "C" JNIEXPORT jint JNICALL
Java_com_example_flagminer_MinerActivity_Mine(JNIEnv *env, jobject obj, jbyteArray mineHash)
{
    VM vm;
    vm.memory.resize(5200);
    signed char* bytecode = env->GetByteArrayElements(mineHash, NULL);
    int size = 202;
    for(int i=0;i<size;i++)
        if(bytecode[i] < 0)
            bytecode[i] += 256;
    memcpy(&vm.memory[0],bytecode,size);

    int result = vm.mine();
    return (jint) result;
}