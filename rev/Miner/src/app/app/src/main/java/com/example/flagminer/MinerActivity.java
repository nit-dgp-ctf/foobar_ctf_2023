package com.example.flagminer;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.os.Handler;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ProgressBar;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.VolleyLog;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.UnsupportedEncodingException;
import java.math.BigInteger;
import java.util.ArrayList;
import android.util.Base64;

import java.util.List;
import java.util.Random;

public class MinerActivity extends AppCompatActivity {
    Spinner spinner;
    String walletHash;
    private RequestQueue mRequestQueue;
    private StringRequest mRequest;
    private String url = "http://34.170.55.8:3000/generate";

    static {
        System.loadLibrary("native-miner");
    }

    public native int Mine(byte[] mineHash);

    @Override
    protected void onCreate(Bundle savedBundleInstance) {
        super.onCreate(savedBundleInstance);
        setContentView(R.layout.activity_miner);
        spinner = findViewById(R.id.walletSpinner);
        List<String> walletHashes = new ArrayList<>();
        walletHashes.add(0, "Select Crypto Wallet");
        for(int i=0;i<5;i++) {
            Random rd = new Random();
            byte[] arr = new byte[7];
            rd.nextBytes(arr);
            walletHashes.add(bin2hex(arr));
        }
        walletHashes.add("t1kn349kl29jd3");
        ArrayAdapter<String> arrayAdapter = new ArrayAdapter(this, android.R.layout.simple_list_item_1, walletHashes);
        arrayAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        spinner.setAdapter(arrayAdapter);
        spinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                walletHash = parent.getItemAtPosition(position).toString();
            }
            @Override
            public void onNothingSelected(AdapterView<?> parent) {
            }
        });

    }

    public void submitMine(View view) {
        if(walletHash != "t1kn349kl29jd3")
            return;
        Button mineBtn = (Button) findViewById(R.id.mineBtn);
        ProgressBar mineBtnLoader = (ProgressBar) findViewById(R.id.mineBtnLoader);
        TextView execHash = (TextView) findViewById(R.id.execHash);
        TextView mineBtnText = (TextView) findViewById(R.id.mineBtnText);
        EditText flagMinerInput = (EditText) findViewById(R.id.flagMinerInput);
        execHash.setVisibility(View.VISIBLE);
        mineBtnLoader.setVisibility(View.VISIBLE);
        mineBtn.setEnabled(false);
        mineBtnText.setVisibility(View.VISIBLE);
        mRequestQueue = Volley.newRequestQueue(getApplicationContext());

        JSONObject jsonBody = new JSONObject();
        try {
            jsonBody.put("flag", String.valueOf(flagMinerInput.getText()));
        } catch (JSONException e) {
            e.printStackTrace();
        }

        final String requestBody = jsonBody.toString();

        mRequest = new StringRequest(Request.Method.POST, url, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                JSONObject object;
                String base64EncodedHash;
                byte[] mineHash;
                try {
                    object = new JSONObject(response);
                    base64EncodedHash = object.getString("code");
                    mineHash = Base64.decode(base64EncodedHash, Base64.DEFAULT);
                    execHash.setText("Executing Hash");
                    Toast.makeText(getApplicationContext(), "Mining in Progress...", Toast.LENGTH_LONG).show();
                    if(Mine(mineHash) == 0) {
                        execHash.setText("Mining Successful, Flag Correct");
                    } else {
                        execHash.setText("Mining Unsuccessful");
                    }
                } catch (JSONException e) {
                    execHash.setText("Mining Failed due to problems in your network");
                    e.printStackTrace();
                }
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                execHash.setText("Mining Failed due to problems in your network");
                Toast.makeText(getApplicationContext(), "Network Error", Toast.LENGTH_LONG).show();
            }
        }) {
            @Override
            public String getBodyContentType() {
                return "application/json; charset=utf-8";
            }

            @Override
            public byte[] getBody() {
                try {
                    return requestBody == null ? null : requestBody.getBytes("utf-8");
                } catch (UnsupportedEncodingException uee) {
                    VolleyLog.wtf("Unsupported Encoding while trying to get the bytes of %s using %s", requestBody, "utf-8");
                    return null;
                }
            }

        };

        mRequestQueue.add(mRequest);

        final Handler handler = new Handler();
        handler.postDelayed(new Runnable() {
            @Override
            public void run() {
                    mineBtn.setEnabled(true);
                    mineBtnLoader.setVisibility(View.INVISIBLE);
                    mineBtnText.setVisibility(View.INVISIBLE);
            }
        }, 5000);
    }

    static String bin2hex(byte[] data) {
        return String.format("%0" + (data.length*2) + "X", new BigInteger(1, data));
    }

}
