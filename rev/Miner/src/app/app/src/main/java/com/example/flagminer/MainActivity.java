package com.example.flagminer;

import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.app.AppCompatDelegate;

import android.content.Context;
import android.os.Bundle;
import android.os.Handler;
import android.view.View;
import android.widget.EditText;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

public class MainActivity extends AppCompatActivity {
    Boolean passwordCheck = false;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        AppCompatDelegate.setDefaultNightMode(AppCompatDelegate.MODE_NIGHT_NO);
        setContentView(R.layout.activity_main);
    }

    public void submitPassword(View view) {
        EditText passwordField = (EditText) findViewById(R.id.passwordField);
        ProgressBar passwordProgress = (ProgressBar) findViewById(R.id.passwordProgress);
        TextView passwordBtnText = (TextView) findViewById(R.id.passwordBtnText) ;
        String password = passwordField.getText().toString();
        passwordField.setEnabled(false);
        view.setEnabled(false);
        passwordBtnText.setVisibility(View.INVISIBLE);
        passwordProgress.setVisibility(View.VISIBLE);

        final Handler handler = new Handler();
        handler.postDelayed(new Runnable() {
            @Override
            public void run() {
                if(!passwordCheck) {
                    passwordField.setEnabled(true);
                    view.setEnabled(true);
                    passwordBtnText.setVisibility(View.VISIBLE);
                    passwordProgress.setVisibility(View.INVISIBLE);
                    Context context = getApplicationContext();
                    CharSequence text = "Wrong Credentials";
                    int duration = Toast.LENGTH_SHORT;

                    Toast toast = Toast.makeText(context, text, duration);
                    toast.show();
                }
            }
        }, 5000);

    }
}