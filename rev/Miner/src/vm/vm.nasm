%include "vm.inc"

; flag = GLUG{jAvA_C_bYT3_CoD3_MiNIn9}
vmovi vr0, 0x00000000; vr0 <- <3><2><1><0>
vmovi vr1, 0x00000000; vr1 <- <7><6><5><4>
vmovi vr2, 0x00000000; vr2 <- <11><10><9><8>
vmovi vr3, 0x00000000; vr3 <- <15><14><13><12>
vmovi vr4, 0x00000000; vr4 <- <19><18><17><16>
vmovi vr5, 0x00000000; vr5 <- <23><22><21><20>
vmovi vr6, 0x00000000; vr6 <- <27><26><25><24>
vmovi vr7, 0x00000000; vr7 <- <31><30><29><28>
vmovi vr8, 0x00000000; vr8 <- <35><34><33><32>

; u32(b'GLUG') ^ 0x1337b17e = 0x5462fd39
vmovi vr11, 0x1337b17e
vmovi vr12, 0x5462fd39
vxor vr0, vr11
vxor vr0, vr12

;u32(b'{jAv') ^ 0x5462fd39(vr12) = 0x22239742
vxor vr1, vr12
vmovi vr12, 0x22239742
vxor vr1, vr12

;u32(b'A_C_') ^ 0xc17ebabe = 0x9e3de5ff

vmovi vr11, 0xc17ebabe
vmovi vr12, 0x9e3de5ff
vxor vr2, vr11
vxor vr2, vr12

;u32(b'bYT3') ^ 0x00001337 = 0x33544a55

vmovi vr11, 0x00001337
vmovi vr12, 0x33544a55
vxor vr3, vr11
vxor vr3, vr12

;u32(b'_CoD') ^ u32(b'3_Mi') = 0x2d221c6c

vxor vr4, vr5;
vmovi vr11, 0x2d221c6c
vxor vr4, vr11

;u32(b'3_Mi') ^ 0x694d5f33 = 0

vmovi vr11, 0x694d5f33
vmovi vr12, 0x00000000
vxor vr5, vr11
vxor vr5, vr12

;u32(b'NIn9') ^ 0x00000009 = 0x396e4947
vmovi vr11, 0x00000009
vmovi vr12, 0x396e4947
vxor vr6, vr11
vxor vr6, vr12

;u32(b'}\x00\x00\x00') ^ 0x00000003 = 0x0000007e
vmovi vr11, 0x00000003
vmovi vr12, 0x0000007e
vxor vr7, vr11
vxor vr7, vr12

vor vr0, vr1
vor vr0, vr2
vor vr0, vr3
vor vr0, vr4
vor vr0, vr5



vend
