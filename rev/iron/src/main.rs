fn check(password: &String) -> i32 {
    println!("Thanks for {}, let me check.....\n", password);
    let ptr_to_byte_array = password.as_bytes();
    if password.len() < 23 {
        return 1;
    } else {
        let res: [u8; 23] = [
            79, 68, 93, 79, 115, 56, 80, 81, 79, 109, 70, 87, 98, 230, 190, 110, 144, 102, 190,
            150, 102, 242, 250,
        ];
        let mut flag = 0;
        for idx in 0..11 {
            if ptr_to_byte_array[idx] ^ 8 != res[idx] {
                flag = 1;
            }
        }
        for idx in 12..23 {
            if (ptr_to_byte_array[idx] << 1) != res[idx] {
                flag = 1;
            } 
        }
        return flag
    }
}

fn main() {
    let mut line = String::new();
    println!("Can you help me pass through the gates of Iron?! :");
    std::io::stdin().read_line(&mut line).unwrap();
    if check(&line) == 1 {
        println!("\n It doesn't seem to work");
    } else {
        println!("\n Thank you so much for your help")
    }
}
