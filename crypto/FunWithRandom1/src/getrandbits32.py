import random
from Crypto.Util.number import *
random.seed(0)

mt = list(random.getstate()[1])
N= 624
M = 397
MATRIX_A = 0x83a2b0c3
UPPER_MASK =  0x80000000  
LOWER_MASK =  0x7fffffff

TemperingMaskB = 0x3f5663d0
TemperingMaskC = 0x56e90000

# 1611182906, 1721268432, 2944249577, 2487212557, 789127738, 4027610014, 1057334138, 2902720905
ele= 43
seven = 12
fif = 67
eigh= 69
mag01 = [0, MATRIX_A]
mt_index = 624
def rand_gen():
    global mt_index
    if mt_index>=N:
        for kk in range(N-M):
            y = (mt[kk]&UPPER_MASK)|(mt[kk+1]&LOWER_MASK)
            mt[kk] = mt[kk+M] ^ (y >> 1) ^ mag01[y & 0x1]

        for kk in range(N-M, N-1):
            y = (mt[kk]&UPPER_MASK)|(mt[kk+1]&LOWER_MASK)
            mt[kk] = mt[kk+(M-N)] ^ (y >> 1) ^ mag01[y & 0x1]

        y = (mt[N-1]&UPPER_MASK)|(mt[0]&LOWER_MASK)
        mt[N-1] = mt[M-1] ^ (y >> 1) ^ mag01[y & 0x1]
        mt_index = 0

    y = mt[mt_index]
    y ^= (y >> ele)
    y ^= (y << seven) & TemperingMaskB
    y ^= (y << fif) & TemperingMaskC
    y ^= (y >> eigh)
    mt_index+=1

    return y

output = []
for _ in range(624):
    y = rand_gen()
    output.append(y)
# print(output)



def untemper(y):
    y = undoTemperShiftL(y)
    y = undoTemperShiftT(y)
    y = undoTemperShiftS(y)
    y = undoTemperShiftU(y)
    return y

def undoTemperShiftL(y):
    last14 = y >> eigh
    final = y ^ last14
    return final

def undoTemperShiftT(y):
    first17 = y << fif
    final = y ^ (first17 & TemperingMaskC)
    return final

def undoTemperShiftS(y):
    a = y << seven
    b = y ^ (a & TemperingMaskB)
    c = b << seven
    d = y ^ (c & TemperingMaskB)
    e = d << seven
    f = y ^ (e & TemperingMaskB)
    g = f << seven
    h = y ^ (g & TemperingMaskB)
    i = h << seven
    final = y ^ (i & TemperingMaskB)
    return final

def undoTemperShiftU(y):
    a = y >> ele
    b = y ^ a
    c = b >> ele
    final = y ^ c
    return final

def get_prev_state(state):
    for i in range(623, -1, -1):
        result = 0
        tmp = state[i]
        tmp ^= state[(i + 397) % 624]
        if ((tmp & UPPER_MASK) == UPPER_MASK):
            tmp ^= MATRIX_A
        result = (tmp << 1) & UPPER_MASK
        
        tmp = state[(i - 1 + 624) % 624]
        tmp ^= state[(i + 396) % 624]
        if ((tmp & UPPER_MASK) == UPPER_MASK):
            tmp ^= MATRIX_A
            result |= 1
        
        result |= (tmp << 1) & LOWER_MASK
        state[i] = result
    
    return state

mt_state = tuple(list(map(untemper, output)) + [0])

prev_state = get_prev_state(list(mt_state[:624]))
print(prev_state)
# random.seed(0)
# print(random.getstate()[1])

# 766982754, 497961170, 3952298588, 2331775348,
# 2487212557, 789127738, 4027610014, 1057334138, 2902720905
