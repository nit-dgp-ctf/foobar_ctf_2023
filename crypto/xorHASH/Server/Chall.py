import base64
import tempfile
import os
from Crypto.Signature import PKCS1_PSS
from Crypto.PublicKey import RSA
from Crypto.Hash import SHA256
import subprocess
import zipfile

public_key_path = "public_key.der"
signature_filename = "signature.bin"
size_limit = 2**20

def read_public_key(filename):
    with open(filename, "rb") as f:
        return RSA.importKey(f.read())

def unzip(zip_file_path, unpack_path):

    with zipfile.ZipFile(zip_file_path) as z:
        z.extractall(unpack_path)

def check_signature(path, public_key):

    hash_value = compute_hash(path + "/somefiles/")
    with open(path + "/" + signature_filename, "rb") as f:
        signature = f.read()
    verifier = PKCS1_PSS.new(public_key)
    return verifier.verify(SHA256.new(hash_value), signature)
    

def xor(str1, str2):
    assert(len(str1) == len(str2))
    result = bytearray((b1 ^ b2 for b1, b2 in zip(str1, str2)))
    return result


def compute_hash(directory):    

    result = bytearray(32)

    for root, dirs, files in os.walk(directory):
        
        for file in files:
            file_path = os.path.join(root,file)
            relative_path = relative_path = file_path.replace(directory, "").encode()
            with open(file_path, "rb") as f:
                h = SHA256.new(relative_path)
                h.update(b"somestuff")
                h.update(f.read())

            result = xor(result, h.digest())
    return result


def execution(path):

    subprocess.run(
        ['python3', 'somefiles/executable.py'],
        cwd=path,
        timeout = 10,
        check=True
    )


def verification(zipfile_path):
    with zipfile.ZipFile(zipfile_path) as f:
        contained_files = f.infolist()
    
    total_size = 0
    for f in contained_files:
        total_size += f.file_size
        if total_size > size_limit:
            raise RuntimeError(f"uncompressed zip file is very large.\nAllowed size: {size_limit}")

    with tempfile.TemporaryDirectory(prefix = "unzip_files") as z:
        unzip(zipfile_path, z)
        public_key = read_public_key(public_key_path)
        valid = check_signature(z, public_key)
        
        if not valid:
            raise RuntimeError("Signature Check Failed")
        else:
            execution(z)


def main():
    os.chdir(os.path.dirname(os.path.abspath(__file__)))

    print("Lets see how good you are in mathematics ;)")
    print("Upload your zipfile in base64 encoded")

    zipfile = input().strip()
    zipfile = base64.b64decode(zipfile)
    print(zipfile)
    print("File is Uploaded")

    with tempfile.NamedTemporaryFile(suffix='.zip') as f:
        f.write(zipfile)
        f.flush()
        
        try:
            verification(f.name)
            print("Execution Complete!!")
        except Exception as e:
            msg = f"An error occured\n{e}"
            print(msg)

if __name__ == '__main__':
    main()
