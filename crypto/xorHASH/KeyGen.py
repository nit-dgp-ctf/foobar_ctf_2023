from Crypto.PublicKey import RSA
from Crypto.Signature import pss
from Crypto.Hash import SHA256


message = b'\xcf\xd3\x8d\x8e\xd2W`\xce9\xd7\xbcev6!\xd4K\x06\x9eLt\xe8\xf2\xf7\x1cG\xec\x9a_\xe2\x80<'

# key = RSA.generate(2048)
# with open('public_key.der', 'wb') as f:
#     f.write(key.publickey().export_key('DER'))

# with open('private_key.der', 'wb') as f:
#     f.write(key.export_key('DER'))

with open('private_key.der', 'rb') as f:
    key = RSA.import_key(f.read())

h = SHA256.new(message)
signature = pss.new(key).sign(h)

with open('signature.bin', 'wb') as f:
    f.write(signature)