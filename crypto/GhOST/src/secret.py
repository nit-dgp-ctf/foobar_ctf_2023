import random

I = random.randint(0, 7)
S_BOX = []

FAVOURITE_ROW: list[int] = [0xa, 0xd, 0x7, 0x9, 0xf, 0x2, 0x0, 0xe, 0x6, 0xb, 0x4, 0x8, 0x3, 0x5, 0xc, 0x1]

base_row = [i for i in range(16)]

for i in range(8):
    random.shuffle(base_row)
    S_BOX.append([*base_row])
try:
    for i in range(8):
        assert S_BOX[i] is not FAVOURITE_ROW
except AssertionError:
    exit()

S_BOX[I] = FAVOURITE_ROW

del FAVOURITE_ROW
