const getRandom = (n) => (
    Math.floor(Math.random() * 4) + 1
)

const secretFn = (str) => {
    let s = getRandom(str.length)
    console.log(s);
    let x = ''
    for (let i = 0; i < str.length; i++) {
        x += String.fromCharCode(str.charCodeAt(i) ^ str.charCodeAt((i + s) % str.length))
    }
    return btoa(x)
}

module.exports = {
    secretFn
}