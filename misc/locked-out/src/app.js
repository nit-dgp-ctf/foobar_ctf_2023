const express = require('express')
const app = express()
const bodyparser = require('body-parser');
const dotenv = require("dotenv")
dotenv.config()
const port = process.env.PORT || 8002
const path = require("path")
const uuid = require('uuid');
const { secretFn } = require('./secret');

const views_path = path.join(__dirname, "./views")

app.use("/src", express.static(path.join(__dirname, "./private"))) // secret.js here
app.set("view engine", "hbs");
app.set("views", views_path)
app.use(bodyparser.json())

const flag = process.env.FLAG

function randomString(str, d) {
    var ans = str.substring(d, str.length) + str.substring(0, d);
    return ans;
}

function XOR(a, b) {
    var res = "",
        i = a.length,
        j = b.length;
    while (i-- > 0 && j-- > 0)
        res = (parseInt(a.charAt(i), 10) ^ parseInt(b.charAt(j), 10)).toString(10) + res;
    return res;
}

app.get('/', (req, res) => {
    const encodedStr = secretFn(flag)
    res.render("index", { data: encodedStr })
})


app.listen(port, () => {
    console.log(`app running on http://localhost:${port}`)
})