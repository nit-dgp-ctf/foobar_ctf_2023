import random

enc = input()
random.seed(2023)
dec = ['a'] * len(enc)

for i in range(8):
    lst = []
    for j in range(8):
        lst.append(enc[(i*8)+j])

    state = [0,1,2,3,4,5,6,7]
    random.shuffle(state)
    dec_lst = ['a'] * 8
    idx = 0
    while idx < 8:
        dec_lst[state[idx]] = lst[idx]
        idx += 1

    for j in range(8):
        dec[(j*8)+i] = dec_lst[j]

print("".join(dec))
