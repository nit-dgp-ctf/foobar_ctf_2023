const jwt = require("jsonwebtoken")
const dotenv = require('dotenv')
dotenv.config()

module.exports = (req,res,next)=>{
    // const authHeader = req.get("Authorization")
    const token = req.cookies;
    console.log(token)
    let decodedToken;
    try{
        decodedToken = jwt.verify(token,process.env.KEY);
    }catch(err){
        req.isAuth = false;
        return next();
    }

    if(!decodedToken){
        req.isAuth = false;
        return next();
    }
    req.isAuth = decodedToken.isAuth;
    req.userId = decodedToken.userId;
    next();
}