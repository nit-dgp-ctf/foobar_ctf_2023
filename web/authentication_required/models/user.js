const mongoose = require('mongoose');
const Schema = mongoose.Schema;

const userSchema = new Schema({
    username:{
        type: String,
        unique: true,
        requried: true,
    },
    password:{
        type: String,
        requried: true
    }
})


module.exports = mongoose.model("User",userSchema);