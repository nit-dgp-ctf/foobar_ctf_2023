const express = require('express');
const bodyparser = require('body-parser');
const { graphqlHTTP } = require('express-graphql');
const mongoose = require('mongoose')

const dotenv = require('dotenv')
dotenv.config()

const graphQlSchema = require("./graphql/schema")
const graphQlResolvers = require("./graphql/resolvers")
const isAuth = require("./middleware/auth")
const User = require('./models/user')
const path = require('path')
const NoIntrospection = require
('graphql-disable-introspection')
const file = require("./flag.json")

const app = express();
const port = process.env.PORT || 8000

app.use(bodyparser.json())

const views_path = path.join(__dirname,"./views")
const static_path = path.join(__dirname,"./public")
app.use(express.static(static_path))
app.set("view engine", "hbs");
app.set("views", views_path);

app.get('/', (req, res) => {
    res.render("login")
})

app.get('/registration',(req,res)=>{
    res.render("register")
})

app.use(isAuth);

app.use('/graphql', graphqlHTTP({
    schema: graphQlSchema,
    rootValue: graphQlResolvers,
    graphiql: false,
    validationRules: [NoIntrospection]
}))

console.log(process.env.DB_URL)
mongoose.connect(`${process.env.DB_URL}`).then( async () => {
    const adminExist = await User.find({username:"admin"})
    // console.log(adminExist)
    if(!adminExist || adminExist.length == 0){
        const user = new User({
            username: "admin",
            password: `${file.flag}`
        })
        await user.save()
    }
    app.listen(port, () => {
        console.log(`server is run on http://localhost:8000`)
    })
}).catch(err=>{
    console.log(err)
})



