const Event = require("../../models/event")
const User = require("../../models/user")
const jwt = require("jsonwebtoken")
const dotenv =require('dotenv')

dotenv.config()

const events = async evnetIds => {
    try {
        const events = await Event.find({ _id: { $in: evnetIds } });
        return events.map(event => {
            return {
                ...event._doc,
                _id: event.id,
                date: new Date(event._doc.date).toISOString(),
                creator: user.bind(this, event.creator)
            };
        });
    } catch (err) {
        throw err;
    }
};

const user = async userId => {
    try {
        const user = await User.findById(userId);
        return {
            ...user._doc,
            _id: user.id,
            createdEvents: events.bind(this, user._doc.createdEvents)
        }
    } catch (error) {
        throw err;
    }
}

module.exports = {
    events: () => {
        return Event.find().then(events => {
            return events.map(event => {
                return {
                    ...event._doc,
                    creator: user.bind(this, event._doc.creator)
                };
            })
        }).catch(err => {
            throw err;
        })
    },
    createEvent: (args,req) => {
        if(!req.isAuth){
            throw new Error("Unauthenticated!")
        }
        const event = new Event({
            title: args.eventInput.title,
            description: args.eventInput.description,
            price: +args.eventInput.price,
            date: new Date(),
            creator: req.userId
        })
        let create;
        return event.save().then(result => {
            create = { ...result._doc, creator: user.bind(this, result._doc.creator) }
            return User.findById(req.userId)
            // return {...result._doc}
        }).then(user => {
            if (!user) {
                throw new Error("User not found!")
            }
            user.createdEvents.push(event)
            return user.save()
        }).then(result => {
            return create
        }).catch(err => {
            console.log(err);
            throw err;
        })
    },
    createUser: (args) => {
        return User.findOne({ username: args.userInput.username }).then(user => {
            if (user) {
                throw new Error("User exists already!")
            }
            const u = new User({
                username: args.userInput.username,
                password: args.userInput.password
            })
            return u.save()
        })
            .then((result) => {
                return { ...result._doc }
            }).catch(err => {
                throw err;
            })
    },
    login: async ({username , password }) => {
        // password = {$regex: `^${password}`}
        // console.log(password.username,username)
        // console.log(password[0])
        if(password[0] === '{'){
            password = JSON.parse(password)
        }
        return User.find({username,password}).then((user)=>{
            // console.log(user)
            // console.log(u);
            if(user.length === 1){
                const token = jwt.sign({userId:user[0].id,username:user[0].username,isAuth:true},process.env.KEY,{expiresIn:"1h"})
                return {userId:user[0].id,token,tokenExpiration:1}
            }else{
                return {userId:null,token:null,tokenExpiration:null}
            }
        }).catch((err)=>{
            throw err;
        })
        
    }
}