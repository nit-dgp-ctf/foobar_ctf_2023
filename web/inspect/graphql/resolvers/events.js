const Event = require('../../models/event');
const User = require('../../models/user');
const Flag = require('../../flag.json')

const objects = [
  {"text":"GLUG{giV3_4_mAN_4_FiSh}"},
  {"text":"GLUG{a_CO1d_daY_1N_HEl1}"},
  {"text":"GLUG{THe_13th_leT7er_ofCoUR$E}"},
  {"text":"GLUG{TH3Re'S_N0_I_1n_teAm}"},
  {"text":"GLUG{fiT_4S_4_FiDdl3}"},
  {"text":"GLUG{I7sNo7brain5UrgERY}"},
  {"text":"GLUG{P4R_FOR_the_c0UrsE}"},
  {"text":"GLUG{JUMpIn9_7H3_6uN}"},
  {"text":"GLUG{R@1n_0N_YouR_P@R@de}"},
  {"text":"GLUG{ShOt_in_THE_D4rK}"},
  {"text":"GLUG{d0Wn_4nD_out}"},
  {"text":"GLUG{THROW_In_7he_TowE1}"},
  {"text":"GLUG{@_buSy_b0dY}"},
  {"text":"GLUG{loNG_in_THE_TO07H}"},
  {"text":"GLUG{ShOt_in_THE_D4rK}"},
  {"text":"GLUG{wHa7_AM_I_Ch0Pped_LIV3R}"},
  {"text":"GLUG{@_FOoL_4nd_Hi5_mon3y_4R3_SOoN_ParTED}"},
  {"text":"GLUG{a_D1Me_a_D0ZeN}"},
  {"text":"GLUG{p1AYinG_P0$sUm}"},
  {"text":"GLUG{wHa7_AM_I_Ch0Pped_LIV3R}"},
  {"text":"GLUG{m0ney_D0E$nT_gRow_oN_7reE$}"},
  {"text":"GLUG{PUT_4_$OCk_1n_i7}"},
  {"text":"GLUG{4_Li7T1E_BIRd_7O1d_m3}"},
  {"text":"GLUG{My_cUP_0f_73a}"},
  {"text":"GLUG{THe_13th_leT7er_ofCoUR$E}"},
  {"text":"GLUG{9rE4Sed_lIGh7nINg}"},
  {"text":"GLUG{CuR1O51TY_kIL1ED_ThE_cAT}"},
  {"text":"GLUG{JACk_OF_@Ll_Trade5_m4$ter_Of_N0N3}"},
  {"text":"GLUG{dR@WIn9_a_B1@nk}"},
  {"text":"GLUG{3vEry_cLoUd_hA5_4_5ilV3r_lIn1n6}"},
  {"text":"GLUG{I7sNo7brain5UrgERY}"},
  {"text":"GLUG{a_f0Ol_anD_hI$_M0N3y_arE_SO0n_Par73D}"},
  {"text":"GLUG{Two_d0Wn_ONE_70_Go}"},
  {"text":"GLUG{b34tING_4roUnd_tHE_8USh}"},
  {"text":"GLUG{Ri6HT_0ff_thE_b47}"},
  {"text":"GLUG{5H0RT_3nD_oF_7H3_5T1Ck}"},
  {"text":"GLUG{m4N_0F_FEw_w0rDS}"},
  {"text":"GLUG{giV3_4_mAN_4_FiSh}"},
  {"text":"GLUG{EV3rYth1NG_BuT_ThE_ki7CH3N_SInK}"},
  {"text":"GLUG{TH3Re'S_N0_I_1n_teAm}"},
  {"text":"GLUG{m0ney_D0E$nT_gRow_oN_7reE$}"},
  {"text":"GLUG{WOUlDNt_harM_@_flY}"},
  {"text":"GLUG{P4R_FOR_the_c0UrsE}"},
  {"text":"GLUG{uNdeR_your_n0sE}"},
  {"text":"GLUG{dR@WIn9_a_B1@nk}"},
  {"flag":Flag.flag},
  {"text":"GLUG{3vEry_cLoUd_hA5_4_5ilV3r_lIn1n6}"},
  {"text":"GLUG{I7sNo7brain5UrgERY}"},
  {"text":"GLUG{d0Wn_4nD_out}"},
  {"text":"GLUG{THROW_In_7he_TowE1}"},
  {"text":"GLUG{@_buSy_b0dY}"},
  {"text":"GLUG{loNG_in_THE_TO07H}"},
  {"text":"GLUG{ShOt_in_THE_D4rK}"},
  {"text":"GLUG{wHa7_AM_I_Ch0Pped_LIV3R}"},
  {"text":"GLUG{@_FOoL_4nd_Hi5_mon3y_4R3_SOoN_ParTED}"},
  {"text":"GLUG{a_D1Me_a_D0ZeN}"},
  {"text":"GLUG{p1AYinG_P0$sUm}"},
  {"text":"GLUG{a_f0Ol_anD_hI$_M0N3y_arE_SO0n_Par73D}"},
  {"text":"GLUG{Two_d0Wn_ONE_70_Go}"},
  {"text":"GLUG{b34tING_4roUnd_tHE_8USh}"},
  {"text":"GLUG{Ri6HT_0ff_thE_b47}"},
  {"text":"GLUG{5H0RT_3nD_oF_7H3_5T1Ck}"},
  {"text":"GLUG{m4N_0F_FEw_w0rDS}"},
  {"text":"GLUG{HIT_BeloW_THe_83lt}"},
  {"text":"GLUG{CuR1O51TY_kIL1ED_ThE_cAT}"},
  {"text":"GLUG{R@1n_0N_YouR_P@R@de}"},
  {"text":"GLUG{JACk_OF_@Ll_Trade5_m4$ter_Of_N0N3}"},
  {"text":"GLUG{d0Wn_4nD_out}"},
  {"text":"GLUG{THROW_In_7he_TowE1}"},
  {"text":"GLUG{@_buSy_b0dY}"},
  {"text":"GLUG{loNG_in_THE_TO07H}"},
  {"text":"GLUG{ShOt_in_THE_D4rK}"},
  {"text":"GLUG{wHa7_AM_I_Ch0Pped_LIV3R}"},
  {"text":"GLUG{@_FOoL_4nd_Hi5_mon3y_4R3_SOoN_ParTED}"},
  {"text":"GLUG{a_D1Me_a_D0ZeN}"},
  {"text":"GLUG{p1AYinG_P0$sUm}"}
]

module.exports = {
  secret:()=>{
    try{
      const s = Flag.flag
      return objects.map(object=>{
        // console.log(object.text)
        return {...object._doc,text:object}
      })
    }catch(err){
      throw err;
    }
  },
  events: async (args) => {
    try {
      console.log(args)
      const events = await Event.find();
      return events.map(event => {
        return {
          ...event._doc,
          _id: event.id,
          date: new Date(event._doc.date).toISOString(),
      };
      });
    } catch (err) {
      throw err;
    }
  },
  createEvent: async (args, req) => {
    const event = new Event({
      title: args.eventInput.title,
      description: args.eventInput.description,
      price: +args.eventInput.price,
      date: new Date(args.eventInput.date)
    });
    let createdEvent;
    try {
      const result = await event.save();
      return {...result._doc};
    } catch (err) {
      console.log(err);
      throw err;
    }
  }
};
