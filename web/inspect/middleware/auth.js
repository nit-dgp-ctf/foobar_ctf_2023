const jwt = require('jsonwebtoken');
const User = require('../models/user')

module.exports = async (req, res, next) => {
  // console.log(req.isAuth,"line 5",req.userId)
  token = req.cookies.jwt
  console.log(token)
  if(!token){
    req.isAuth = false;
    next()
  }
  const decodedToken = jwt.verify(token,"UJJLNDswk9PUzy6b")

  const user = await User.findOne({username:decodedToken.username})
  // console.log(user,decodedToken)
  if(!user){
    req.isAuth = false
    return new Error("User is not Found")
  }
  req.locals = {check:true}
  req.isAuth = true;
  req.userId = decodedToken.userId;
  next();
};
